//------------------------------------------------------------> initialisation de socket.io coté client
var socket = io();

$('form').submit(function(e){
    e.preventDefault(); //-------------------------------------> evite le rechargemeent de la page a chaque envoie

    // Creation objet dans JSON du message
    var message = {
        text : $('#m').val()
    }
    socket.emit('chat-message', message); //-------------------> émet l'evenement avec le message 
    $('#m').val('');//-----------------------------------------> champs input vide
    if (message.text.trim().length !== 0){ //------------------> gestion message vide
        socket.emit('chat-message', message);
    }
    $('#chat input').focus(); 

   
})
socket.on('chat-message', function(message){ //----------------> afficher les messages
    $('#message').append($('<li>').text(message.text));
});