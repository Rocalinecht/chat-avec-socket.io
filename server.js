//------------------------------------------------------------> initialisation du serveur 
var express = require ('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

//------------------------------------------------------------> chemin pour accèder au fichier public 
app.use(express.static("PUBLIC"));

//------------------------------------------------------------> evenement connexion 
io.on('connection', function(socket){
    console.log(' a user is connected');
    socket.on('disconnect', function(){   //------------------> evenement de deconnexion
        console.log (' user disconnected')
    })

//------------------------------------------------------------> RECEPTION DES EVENEMENT MESSAGE
socket.on('chat-message', function(message){
    console.log('message : '+ message.text);
});

//------------------------------------------------------------> emmetre les message a tout les utilisateur
socket.on('chat-message', function(message){
    io.emit('chat-message',message);
});
});

//------------------------------------------------------------> lancement du server 
http.listen(3000, function(){
    console.log('Server en route sur le port 3000')
});